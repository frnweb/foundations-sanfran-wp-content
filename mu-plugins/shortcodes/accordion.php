<?php
// ACCORDION Dependent on ZURB Foundation 6
	function sl_accordion( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> ''
		), $atts );
		return '<ul class="accordion sl_accordion sl_accordion--' . esc_attr($specs['class'] ) . '" data-accordion data-multi-expand="true" data-allow-all-closed="true">' . do_shortcode ( $content ) . '</ul>';
	}
	add_shortcode ('accordion', 'sl_accordion' );
///ACCORDION

// ACCORDION ITEM
	function sl_accordion_item ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'title'		=> ''
			), $atts );
		static $i = 0;
		$i++;
		$content = wpautop(trim($content));
		$value = '<li class="accordion-item sl_accordion__item" data-accordion-item><a href="#" class="accordion-title sl_accordion__title">' . esc_attr($specs['title'] ) . '</a><div id="panel' . $i . '" class="accordion-content sl_accordion__content" data-tab-content>' . do_shortcode( $content ) . '</div></li>';

		return $value;
	}

	add_shortcode ('accordion-item', 'sl_accordion_item' );
///ACCORDION ITEM
?>