<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$staff = new FieldsBuilder('staff');

$staff
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))
		->addFields(get_field_partial('partials.grid_options'));


$staff
	->addTab('module_type', ['placement' => 'left'])
		->addSelect('type_select', [
			'label' => 'Module Type',
			'wrapper' => ['width' => 20]
		])
	  	->addChoices(
	  	['slider' => 'Duo Slider'],
	  	['deck' => 'Deck']
		);

$staff
	->addTab('duo_content', [
		'placement' => 'left',
		'conditional_logic' => [
				[
					[
					'field' => 'type_select',
					'operator' => '==',
					'value' => 'slider',
					],
				],
			],
	])

	// Card
		->addFields(get_field_partial('modules.card'));

$staff
	->addTab('staff_select', ['placement' => 'left'])
	//Staff Picker
    ->addRelationship('staff', [
        'label' => 'Staff Members',
        'post_type' => 'sl_staff_cpts',
        'min' => 1,
    ])
    ->setInstructions('Choose multiple staff members to highlight');
    

return $staff;