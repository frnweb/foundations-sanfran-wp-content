<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$builder = new FieldsBuilder('builder');

$builder
   	->addFlexibleContent('modules', [
   		'button_label' => 'Add Module'
   	])
	    ->addLayout(get_field_partial('modules.alert'))
	    ->addLayout(get_field_partial('modules.billboard'))
		->addLayout(get_field_partial('modules.carousel'))
		->addLayout(get_field_partial('modules.deck'))
		->addLayout(get_field_partial('modules.duo'))
		->addLayout(get_field_partial('modules.faq'))
		->addLayout(get_field_partial('modules.insurance'))
		->addLayout(get_field_partial('modules.location'))
		->addLayout(get_field_partial('modules.modal'))
		->addLayout(get_field_partial('modules.post_content'))
   		->addLayout(get_field_partial('modules.spotlight'))
		->addLayout(get_field_partial('modules.staff'))
		->addLayout(get_field_partial('modules.testimonial'))
		->addLayout(get_field_partial('modules.related_articles'))
		->addLayout(get_field_partial('modules.wildcard'))
		->addLayout(get_field_partial('modules.wysiwyg'))
		->addLayout(get_field_partial('modules.wrapper_open'))
		->addLayout(get_field_partial('modules.wrapper_close'))
		;
return $builder;