<?php

/**
 * Slate required files
 * Paths are relative to the app/ directory
 */

$slate_includes = array(
		'/setup.php',				// Setup functions
		'/app.php',					// Timber class
		'/../vendor/autoload.php',	// Grab the autoload from composer								
		'/actions.php',
		'/helpers.php',
);

foreach ( $slate_includes as $file ) {
	$filepath = locate_template( '/app' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /app%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

//Redirect FAQ Post Type to FAQ Page
add_action( 'template_redirect', 'faq_redirect_post' );

function faq_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  if ( is_single() && 'sl_faqs_cpts' ==  $queried_post_type ) {
    wp_redirect( '/faq/', 301 );
    exit;
  }
}

// Breadcrumbs
function get_breadcrumb() {
       
    // Settings
    $separator          = '&gt;';
    $breadcrums_id      = 'sl_breadcrumbs';
    $breadcrums_class   = 'sl_breadcrumbs';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="sl_breadcrumbs__home"><a class="sl_breadcrumbs__link show-for-medium" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a><a class="hide-for-medium" href="' . get_home_url() .'"><i class="fa fa-home"></i></a></li>';
        
        if ( is_home() && !is_front_page()) {
        	echo '<li class="sl_breadcrumbs__current">Articles</li>';

        } 
        
        else if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="sl_breadcrumbs__current">' . post_type_archive_title( '', false ) . '</li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {

                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);
                  
                    echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="sl_breadcrumbs__current">' . $custom_tax_name . '</li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                //Staff Custom Post Type
                if($post_type == 'sl_staff_cpts') {
                    echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="/about/staff/" title="Our Staff">Our Staff</a></li>';
                } 

                //All other custom post types
                else {
                    echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                }
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="sl_breadcrumbs__current">' . get_the_title() . '</li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="sl_breadcrumbs__current">' . get_the_title() . '</li>';
            
            // else get the title
            } else  {
                  
                echo '<li class="sl_breadcrumbs__current">' . get_the_title() . '</li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="sl_breadcrumbs__current">' . single_cat_title('', false) . '</li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="sl_breadcrumbs__parent sl_breadcrumbs__parent--' . $ancestor . '"><a class="sl_breadcrumbs__link" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="sl_breadcrumbs__current">' . get_the_title() . '</li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="sl_breadcrumbs__current">' . get_the_title() . '</li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="sl_breadcrumbs__current">' . $get_term_name . '</li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="sl_breadcrumbs__current">'.__('Page') . ' ' . get_query_var('paged') . '</li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="sl_breadcrumbs__current">Search Results</li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li class="sl_breadcrumbs__current">' . 'Page Not Found' . '</li>';
        }
       
        echo '</ul>';
           
    }//end if is not front page
       
}//end function get_breadcrumbs